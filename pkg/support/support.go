/*
Package support contains common support utilities that can be used in multiple demos
*/
package support

// by convention the package name should be the same as the containing directory

import (
	"fmt"
	"strings"
)

// General visibility rule: if a name begins with captial letter, it is exported.
// Otherwise it is visible only in the same package.

// note: it is mandatory (you get linting errors from go vet) to document all exported names
// AND to start the documentation with exported name

var sepString = strings.Repeat("-", 120)

// PrintSepFmt prints a separator using fmt.Println
func PrintSepFmt() {
	fmt.Println(sepString)
}

// PrintSep prints a separator using the provided printing function from a log-compatible package
func PrintSepLog(p func(a ...interface{})) {
	p(sepString)
}
