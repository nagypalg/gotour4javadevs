package main

import (
	"net/http"
	"net/http/httptest"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

// a simple test
func TestRouter(t *testing.T) {
	assert := assert.New(t)
	w := httptest.NewRecorder()
	req := httptest.NewRequest(http.MethodGet, "/greet", nil)
	router().ServeHTTP(w, req)

	expected := "Hello, world!"
	actual := w.Body.String()
	assert.Equal(expected, actual)
}

// a "table driven" test (same test logic is executed with many input/output combinations)
func TestPort(t *testing.T) {
	assert := assert.New(t)
	oldArgs := os.Args
	// note how we avoid side effects on os.Args (that may be used by other tests)
	defer func() { os.Args = oldArgs }()

	// an slice of anonymous structs
	porttests := []struct {
		args     []string
		expected int
	}{
		{[]string{"cmd", "7070"}, 7070},
		{[]string{"cmd", "7070", "xxx"}, 7070},
		{[]string{"cmd"}, 8000},
		{[]string{"cmd", "abc"}, 8000},
	}

	for _, tt := range porttests {
		os.Args = tt.args
		actual := port()
		assert.Equal(tt.expected, actual)
	}

}
