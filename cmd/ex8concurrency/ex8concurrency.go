/*
Demos Go's concurrency capabilities: goroutines, channels and mutex
*/
package main

import (
	"log"
	"strconv"
	"strings"
	"sync"
	"time"

	sup "gitlab.com/nagypalg/gotour4javadevs/pkg/support"
)

func init() {
	// Also the standard library logger can be configured
	// (although more limited than logrus)
	// see https://golang.org/pkg/log/#pkg-constants
	log.SetFlags(log.Ldate | log.Ltime | log.Lmicroseconds | log.Lshortfile)
}

func callFiboClose(ccap, n int) {
	// Goroutines (lightweight threads) communicate via channels
	// we can send ccap messages to the channel before it gets blocked
	// By default channels have 0 capacity, i.e. the sender blocks so long until the receiver reads
	c := make(chan int, ccap)
	log.Println("Channels capacity:", cap(c))
	numFibo := cap(c) + n
	// Starting a goroutine is that easy!
	go fiboClose(numFibo, c)
	// yet another way to create an empty slice (zero length, zero capacity)
	results := []string{}
	// read from the channel until it is closed
	for i := range c {
		results = append(results, strconv.Itoa(i))
	}
	log.Printf("First %d fibo numbers: %s", numFibo, strings.Join(results, ","))
}

// sends Fibonacci numbers to the provided channel
func fiboClose(n int, c chan int) {
	x, y := 0, 1
	for i := 0; i < n; i++ {
		// write to a channel
		c <- x
		x, y = y, x+y
	}
	// it is NOT best practice to close channels
	close(c)
}

// send Fibonacci numbers until a quit signal comes
func fiboQuit(c, quit chan int) {
	x, y := 0, 1
	// empty for = infinite loop
	for {
		// select lets a goroutine wait on multiple communication operations
		select { // it will block so long until one of the options become available
		case c <- x: // we could send x to channel c (was not blocked, i.e. the receiver has read the previous value)
			x, y = y, x+y
		case <-quit: // OR we received the quit signal
			log.Println("quit received")
			return
		}
	}
}

func callFiboQuit(n int) {
	c := make(chan int) // channel with zero capacity (will immediately block)
	quit := make(chan int)
	// start an anonymous function
	go func() {
		for i := 0; i < n; i++ {
			// read from channel c
			log.Println("Received", <-c)
		}
		// write to channel quit
		quit <- 0
	}()
	fiboQuit(c, quit)
}

func goBomb() {
	// creates a channel that ticks every 100 ms
	tick := time.Tick(100 * time.Millisecond)
	// creates a channel that ticks once after 500 ms
	boom := time.After(500 * time.Millisecond)
	for {
		select { // note: this will not block because we have default case
		case <-tick: // called when the tick channel has data
			log.Println("tick.")
		case <-boom: // called when the boom channel has data
			log.Println("BOOM!")
			return
		default: // called when neither the tick or the boom channel has data
			log.Println("    .")
			time.Sleep(50 * time.Millisecond) // we have to sleep because select does not block
		}
	}
}

// SafeCounter is safe to use concurrently.
type SafeCounter struct {
	v          map[string]int // zero value is nil, we have to initialize!
	sync.Mutex                // zero value is an unlocked mutex, which is OK for most cases
	// we have embedded Mutex so that we can call Lock and Unlock directly on SafeCounter
}

// Inc increments the counter for the given key.
func (c *SafeCounter) Inc(key string) {
	// note: can directly use the mutex methods!
	c.Lock()
	defer c.Unlock()
	c.v[key]++
}

// Value returns the current value of the counter for the given key.
func (c *SafeCounter) Value(key string) int {
	c.Lock()
	defer c.Unlock()
	return c.v[key]
}

func demoSafeCounter() {
	// note how we can name the struct fields and then we do not have to initialize them all
	c := SafeCounter{v: make(map[string]int)}
	for i := 0; i < 1000; i++ {
		go c.Inc("somekey")
	}

	time.Sleep(time.Second)
	log.Println("Safe counter value:", c.Value("somekey")) // obviously will be 1000
}

func main() {
	callFiboClose(5, 0)
	sup.PrintSepLog(log.Println)

	callFiboClose(5, 5)
	sup.PrintSepLog(log.Println)

	goBomb()
	sup.PrintSepLog(log.Println)

	demoSafeCounter()
	sup.PrintSepLog(log.Println)

	callFiboQuit(3)
}
