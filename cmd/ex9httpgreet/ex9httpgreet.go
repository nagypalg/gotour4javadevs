/*
A simple HTTP server that greets users on /greet
*/
package main

import (
	"net/http"
	"os"
	"strconv"
	"time"

	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"
)

const defaultPort = 8000

func main() {
	log.Fatal(http.ListenAndServe(":"+strconv.Itoa(port()), router()))
}

func port() (portnum int) {
	var err error
	portnum = defaultPort
	if len(os.Args) > 1 {
		provided := os.Args[1]
		portnum, err = strconv.Atoi(provided)
		if err != nil {
			log.Errorf("Could not parse provided port %s, using default", provided)
			portnum = defaultPort
		}
	}
	log.Printf("httpgreet is going to listen on port %d\n", portnum)
	return
}

func router() http.Handler {
	r := mux.NewRouter()
	r.Path("/greet").Methods(http.MethodGet).HandlerFunc(greet)
	return r
}

func greet(w http.ResponseWriter, req *http.Request) {
	now := time.Now().Format(time.ANSIC)
	log.Infof("Starting serving greet request at %s", now)
	w.Write([]byte("Hello, world!"))
	now = time.Now().Format(time.ANSIC)
	log.Infof("Done serving greet request at %s", now)

}
