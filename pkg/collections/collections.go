/*
Package collections contains examples about slices and maps
*/
package collections

import (
	"strings"

	log "github.com/sirupsen/logrus" // external library for logging
	// note that we have gave the package an alias so that we can refer to it
	// with the same name as the standard library
	// also note that it is a drop-in replacement for the built-in logging that way
	// this is a pattern that many libraries follow - they extend the standard library

	sup "gitlab.com/nagypalg/gotour4javadevs/pkg/support"
)

func init() {
	log.SetLevel(log.DebugLevel)
	log.SetFormatter(&log.TextFormatter{
		DisableColors: true,
		FullTimestamp: true,
	})
}

// WordCount counts the words in the given string
func WordCount(s string) map[string]int {
	// words is a slice of strings: []string
	// this is like an ArrayList<String> (there is a backing array)
	words := strings.Fields(strings.ToLower(s))

	// this is like a Map<String, Integer>
	res := make(map[string]int)

	// the first return value of range would be the slice index but we do not need it so we ignore it
	for _, word := range words {
		// note: we do not need to initialize res[word] because if there is no value in the map,
		// we get the zero value of int, which is 0 - which is OK for our use case
		res[word] = res[word] + 1
	}

	return res
}

func logWordCountResult(s string) {
	log.Infof("Word count of '%s': %v", s, WordCount(s))
}

// RunExamples runs examples in the collections package
func RunExamples() {
	// this could be a standard log library call
	log.Println("Examples of collections package")
	sup.PrintSepLog(log.Println)

	logWordCountResult("Hello Go")
	sup.PrintSepLog(log.Println)

	logWordCountResult("Hello Go Go")
	sup.PrintSepLog(log.Println)

	logWordCountResult("Hello Go Go Go Java")
}
