// Example 1: a simple "hello world" program on steroids

// Executable Go programs must be in package main
package main

// Go organizes code in "packages"
import (
	// these are packages of the standard library
	"fmt"
	"os"

	// this is an external package
	// by convention these are loaded by their URL (which usually works in the browser)
	// by default packages can be referred to by their last path element in their URL (in this case 'quote')
	"rsc.io/quote"
)

// the first element of os.Args is the name of the command
var cmd = os.Args[0]

// Executable Go programs must have main function
// Note that it has no arguments and no return value
func main() {
	fmt.Println("Hello from", cmd)
	// there are no semicolons either (automatically appended by the compiler)
	fmt.Println("Árvíztűrő tükörfúrógép.") // Full UTF-8 support
	// Go does not have characters but UTF-8 "runes"
	fmt.Printf("Go quote of today: '%s'\n", quote.Go())
	// Go quote of today: 'Don't communicate by sharing memory, share memory by communicating.'
}

// The init method will be executed before the main method.
// Variables defined at the package level can be used.
func init() {
	fmt.Printf("Init of %s\n", cmd)
}
