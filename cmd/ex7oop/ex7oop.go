/*
Shows how Go's "lightweight" OOP works
*/
package main

import (
	"encoding/json"
	"fmt"
	"log"
	"math"
	"reflect"
	"strings"

	"gitlab.com/nagypalg/gotour4javadevs/pkg/collections"
	sup "gitlab.com/nagypalg/gotour4javadevs/pkg/support"
)

// It is idiomatic Go to define small interfaces
type abser interface {
	abs() float64
}

func logAbs(v abser) {
	log.Printf("Abs(%v)=%v", v, v.abs())
}

// Vertex represents a 2D point
type Vertex struct {
	// Go does not have classes, only structs
	X float64 `json:"x"` // Go also have "annotations"
	Y float64 `json:"y"` // they are called tags: https://medium.com/golangspec/tags-in-golang-3e5db0b8ef3e
	// go vet complains about annotation strings on unpublished items, so we publish Vertex
}

// we can have methods on structs
// methods are simply functions with a receiver
// for structs best practice is to use pointer receivers to avoid copying the whole struct
// (Go always passes parameters by value - such like Java)
func (v *Vertex) abs() float64 {
	// look ma, no NPE! :)
	if v == nil {
		return 0
	}
	return math.Sqrt(v.X*v.X + v.Y*v.Y)
}

// Go's approach is similar to "duck typing"
// if a type has the right methods, it can be used where the interface is expected
// no implements keyword is needed!
// Implicit interfaces decouple the definition of an interface from its implementation,
// which could then appear in any package without prearrangement.
// With the above method *Vertex will "implement" the abser interface (but NOT Vertex!)

// if we want to change the receiver struct, we have to use pointer receiver anyway,
// otherwise we are just changing a copy
// but if we return the changed copy (like here) that also works for value receivers if the caller saves it
// it is also best practice not to mix pointer and value receivers for one type
func (v *Vertex) scale(f float64) *Vertex {
	if v == nil {
		return nil
	}
	v.X = v.X * f
	v.Y = v.Y * f
	return v
}

// type alias
type myFloat float64

// methods are possible on all types (incl. functions!) in the same package
// this is basically extension methods on built-in types!
// With this method myFloat will also "implement" the abser interface
func (f myFloat) abs() float64 {
	if f < 0 {
		return float64(-f)
	}
	return float64(f)
}

type wordMap map[string]int

// wordMap also "implements" the abser interface
func (m wordMap) abs() float64 {
	sum := 0
	for _, v := range m {
		sum += v
	}
	// sum is int here so we have to convert
	return float64(sum)
}

// just to be compatible with Java :)
func (m wordMap) size() int {
	return len(m)
}

// Go does not have keySet or values like Java collections
// but in most cases we do not need them either
// because we can iterate with for key, value := range map {...}
func (m wordMap) keys() []string {
	// another slice example, here we make one with zero length and len(m) capacity
	// this means that the underlying array will be initialized with len(m) length
	// we just will not see it in the slice initially
	keys := make([]string, 0, len(m))
	// here we need only the keys so omit the second variable
	for k := range m {
		// another way to add elements to a slice
		keys = append(keys, k)
	}
	return keys
}

// Implementing the https://golang.org/pkg/fmt/#Stringer interface
// is Go's toString() method
// It is a nice example of an interface in another package which is "implemented" elsewhere
func (m wordMap) String() string {
	return strings.Join(m.keys(), ",")
}

func (v *Vertex) String() string {
	if v == nil {
		return "<nil>"
	}
	return fmt.Sprintf("(%f,%f)", v.X, v.Y)
}

type vertex3d struct {
	// this is NOT inheritance but composition with automatic delegation
	// called embedding
	// it will expose all the fields and methods of Vertex
	// but the receiver will be Vertex, not vertex3d!
	Vertex
	z float64
}

func (v *vertex3d) String() string {
	if v == nil {
		return "<nil>"
	}
	return fmt.Sprintf("(%f,%f,%f)", v.X, v.Y, v.z)
}

func make3dvertex(x, y, z float64) *vertex3d {
	return &vertex3d{Vertex{x, y}, z}
}

// interface{}, the empty interface is Go's java.lang.Object
func describe(i interface{}) {
	log.Printf("(%v, %T)\n", i, i)
}

func main() {
	demoVertex()
	sup.PrintSepLog(log.Println)

	demoVertex3d()
	sup.PrintSepLog(log.Println)

	demoMyFloat()
	sup.PrintSepLog(log.Println)

	demoAbser()
	sup.PrintSepLog(log.Println)

	demoWordMap()

	demoSimpleTypeReflect()
	sup.PrintSepLog(log.Println)

	demoStructReflect()
}

func demoVertex() {
	var v *Vertex
	// note: we can safely use %s because we have provided a String() method
	// if we would not have provided, we would still not get an error but the result would be suboptimal
	// similarly to the case when we forget to provide a toString() method in Java
	log.Printf("Vertex's zero value: %s", v)
	v = &Vertex{3, 4}
	log.Printf("Vertex's non-zero zero value: %s", v)
	// %+v will print field names for structs, here {X:3 Y:4}
	log.Printf("Abs(%+v)=%f\n", *v, v.abs())
	// look, method receiver are auto-converted between pointer and non-pointer forms
	// %#v will print the full Go sytax of the value, here &main.Vertex{X:3, Y:4}
	log.Printf("Abs(%#v)=%f\n", v, (*v).abs())
	v.scale(10)
	log.Printf("Abs(%v)=%f\n", v, v.abs())
	logAbs(v)
	describe(v)
	// This will use the JSON "annotations"!
	vjson, _ := json.Marshal(v)
	log.Println("Vertex JSON:", string(vjson)) //Vertex JSON {"x":30,"y":40}
}

func demoAbser() {
	var v *Vertex
	// An interface is a (value, type) tuple
	var a abser
	// zero value: (nil, nil), we should NOT call methods on it
	// a.abs() would cause a Go panic
	describe(a) //(<nil>, <nil>)
	a = v
	describe(a) //(<nil>, *oop.Vertex)
	// Although the value is still nil, we can call abs because Go has the type and knows which method to call
	logAbs(a) //Abs(<nil>)=0
	a = &Vertex{1, 2}
	describe(a) //((1.000000,2.000000), *main.Vertex)
	// if a variable is an interface type, we can safely convert it to other types
	// note: for non-interface types we have compile-time checking!
	s, ok := a.(fmt.Stringer)
	log.Println("Conversion to fmt.Stringer was successful:", ok) //true
	describe(s)                                                   //((1.000000,2.000000), *main.Vertex)
}

func demoMyFloat() {
	mf := myFloat(-math.Sqrt2)
	log.Println(mf.abs())
	// we can call logAbs because we "implement" abser. This is a compile-time check!
	logAbs(mf)
	describe(mf)
}

func demoSimpleTypeReflect() {
	var x myFloat = 3.4
	log.Println("type:", reflect.TypeOf(x))                 //type: main.myFloat
	log.Println("value:", reflect.ValueOf(x).String())      //value: <main.myFloat Value>
	log.Println("value kind:", reflect.ValueOf(x).Kind())   //value kind: float64
	log.Println("float value:", reflect.ValueOf(x).Float()) //float value: 3.4
}

func demoStructReflect() {
	v := Vertex{11, 12}
	vt := reflect.TypeOf(v)
	log.Println("Vertex fields:")
	// only published fields will be included!
	for i := 0; i < vt.NumField(); i++ {
		field := vt.Field(i)
		log.Printf("Field: name=%s, type=%s, JSON tag=%s", field.Name, field.Type, field.Tag.Get("json"))
	}
	log.Println("Vertex methods:")
	// note that only &Vertex has methods!
	vt = reflect.TypeOf(&v)
	// only published methods will be included!
	for i := 0; i < vt.NumMethod(); i++ {
		m := vt.Method(i)
		log.Printf("Method: name=%s, type=%s", m.Name, m.Type)
	}

}

func demoWordMap() {
	handleWordCount("Go go go", "go")
	handleWordCount("Go go go", "java")
	handleWordCount("Go go Java", "java")
}

func demoVertex3d() {
	v3d := make3dvertex(4, 5, 6)
	// note: we can use Vertex fields directly!
	log.Printf("3D coordinates: x=%f, y=%f, z=%f\n", v3d.X, v3d.Y, v3d.z)
	// and all methods, as well -> but beware because these will ignore the new field, z
	logAbs(v3d)
	// but here we have overridden the method, so will display all fields
	log.Printf("vertex3d as string: %s\n", v3d) // vertex3d as string: (4.000000,5.000000,6.000000)
	describe(v3d)
}

func handleWordCount(s string, w string) {
	wcMap := wordMap(collections.WordCount(s))
	// because wcMap is of type wordMap, we now can use all the methods we have defined
	log.Printf("We have %d different words in '%s': %v", wcMap.size(), s, wcMap.keys())
	log.Printf("We have %d words in '%s'", int(wcMap.abs()), s)
	// map access return two values, the second one shows whether the value actually was in the map
	// this is needed to check whether the zero value of type was stored in the map vs. it was missing
	if count, present := wcMap[w]; present {
		// count and present are visible only in the if and else blocks
		log.Printf("Word count of '%s' in '%s' is %d", w, s, count)
	} else {
		log.Printf("Word '%s' is not in '%s'", w, s)
	}
	// note the %s notation, we are using the String() method here
	log.Printf("Word map: %s", wcMap)
	sup.PrintSepLog(log.Println)
}
