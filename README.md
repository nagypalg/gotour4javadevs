# Go Tour for Java Developers

## What is this?

Go code walkthrough based on and inspired by the [Tour of Go](https://tour.golang.org/) for Java developers.

## Why should you care?

In my opinion Java's success in the enterprise world is based on its stability 
(backwards compatibility) and on its relative simplicity, which leads to readability.

[Quote from Robert C. Martin's Clean Code book](https://www.goodreads.com/quotes/835238-indeed-the-ratio-of-time-spent-reading-versus-writing-is): 
```
Indeed, the ratio of time spent reading versus writing is well over 10 to 1. 
We are constantly reading old code as part of the effort to write new code. 
...[Therefore,] making it easy to read makes it easier to write.
```

It is also my personal experience that although Java is definitely verbose and sometimes
usable only with an IDE (just think of all those getters/setters/equals methods!), it is
possible to maintain software that was written by other developers in many-many years.

The main design goal of Go was to create a **scalable language**, where scalability can be interpreted in three dimensions:
1. **In terms of precessing units**: Go provides very good concurrency support and a powerful http library to implement both services and clients accessing those services.
2. **In terms of SW size (LOC)**: Go has some interesting concepts for structuring code and using external dependencies. It also compiles extremely fast.
3. **In terms of developer team size**: Go's simplicity and readability is of paramount importance here. Go also enforces some enterprise best practices such as consistent code formatting and documentation.
 
Especially due to its simplicity and readability I felt that Go could be a viable alternative to Java in the enterprise world.

[A quote attributed to Einstein](https://quoteinvestigator.com/2011/05/13/einstein-simple/): 
```
Everything should be made as simple as possible, but not simpler.
```
Let's see whether it is true for Go and whether it can be a worthy challenger of Java in the enterprise world.

## Structure of this walkthrough

Executable examples are in the `cmd` directory, packages (kind of libraries) in the `pkg` directory.

Examples begin with "ex" and continue with a number. It is recommended that you look at them in order. 

The source code is heavily commented, so if you are a seasoned Java (or JavaScript, Python, ...) programmer, you should have no issues following them.

* __ex1hello__: a simple "hello world" program
* __ex2varfunc__: introduces variables, constants and functions
* __ex3control__: introduces control structures: if, for and switch
* __ex4collections__: introduces slices and maps
* __ex5hifunc__: demos higher order functions
* __ex6defer__: shows a special Go construct, defer, which is very handy in day-to-day error handling
* __ex7oop__: although Go is NOT an OOP language, it has some constructs that make it feel like one
* __ex8concurrency__: one of the main features of Go is the built-in concurrency support, this example introduces that
* __ex9httpgreet__: a simple hello world HTTP server, and it also shows how to test in Go

## Conclusion

Go's standard library has features that did not make it to this brief walkthrough like a powerful templating engine, so if you miss some feature, be sure to check the standard library which may already have a solution.

**I personally very much liked what I saw and I do think that Go is a worthy challenger to Java.** 

Go is definitely more concise and easier to read than Java, it almost feels like reading Python code just without the strange formatting conventions. 
Although it is really a simple language, it did not feel too simple. I could not tell any Java feature where Go does not provide an equivalent, sometimes simpler and more elegant solution. 
Having functions as first class citizens is definitely great and much more powerful than Java's afterthought called lambdas. Multi-valued returns are really useful, especially for error handling. 
Go also has a vivid ecosystem (such like Java) and a very strong standard library. It is indeed a "batteries included" language.

Mandating consistent formatting and documentation of the public interface of packages is a great idea and makes life much easier in an enterprise setting. 
As a side effect, most open source code I was looking at so far was very well documented, which is a completely different experience from what I normally see in the Java ecosystem. 
[The documentation of the standard library](https://golang.org/pkg/) is just amazing. Especially the built-in support for code examples helps a lot. 
I think it is fairly easy to learn Go and most developers can be productive in weeks, if not in days. Or in [minutes](https://learnxinyminutes.com/docs/go/). ;)

I do not have enough experience to decide whether not having proper OOP (classes and inheritance) is a bug or a feature. 
Interfaces and implicit implementation (a kind of duck typing) may be enough, time will tell. 

It is definitely a huge plus for Go that it is a statically typed language (such like Java), which  is (in my opinion) a must for any language that is used for writing bigger programs (in terms of LOC).

If you are ready to pay, **IDE support** for Go is as good as for Java: IntelliJ provides Goland which has all the bells and whistles. 
If you want to have it free, VS Code is also a decent IDE with code completion but with Go 1.12 renaming did not work for me at all and I completely missed other refactorings, such as extract method or variable. 
Perhaps it is just me, I am not a VS Code power user and may have configured something wrong, so your mileage may vary.
It is important to say, however, that due to the concise nature of Go IDE support is much less important than for Java. 
You can write Go code even in vim (which was not most popular editors for the language some years ago!), which I would never ever try to do in Java.

Although Java still has the advantage of "compile once run everywhere", Go supports cross-compilation. E.g. you can build a linux binary under Windows just by executing `set GOOS=linux` before `go build`.
And Go is much more Docker-friendly (and in general easier to deploy) because the resulting file is a single binary file that does not depend on anything else. 

[A quote from Martin Odersky, creator of Scala](http://go-lang.cat-v.org/quotes):
```
I like a lot of the design decisions they made in the [Go] language. Basically, I like all of them.
```



