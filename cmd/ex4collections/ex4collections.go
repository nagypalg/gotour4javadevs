package main

import (
	"gitlab.com/nagypalg/gotour4javadevs/pkg/collections"
)

func main() {
	collections.RunExamples()
}
