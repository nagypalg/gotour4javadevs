/*
Shows higher order functions
*/
package main

import (
	"log"
	"strconv"
	"strings"

	sup "gitlab.com/nagypalg/gotour4javadevs/pkg/support"
)

// fibonacci is a function that returns
// a function that returns an int, i.e. it is a higher order function
func fibonacci() func() int {
	a, b := 0, 1
	// functions are closures
	return func() int {
		res := a
		a, b = b, a+b
		return res
	}
}

// functions are first class citizens, so can be passed as parameters, too
func executeTimes(times int, f func() int) string {
	// another example for a slice, this makes a slice with the size of times
	results := make([]string, times)
	for i := 0; i < times; i++ {
		results[i] = strconv.Itoa(f())
	}
	return strings.Join(results, ",")
}

func main() {
	log.Println(executeTimes(10, fibonacci()))
	sup.PrintSepLog(log.Println)

	log.Println(executeTimes(20, fibonacci()))
	sup.PrintSepLog(log.Println)

	// Example for anonymous function, similar to Java8 lambda expressions
	log.Println(executeTimes(5, func() int { return 2 }))

}
