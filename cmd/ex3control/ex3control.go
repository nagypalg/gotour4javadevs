/*
Introduces Go's controls structures: if, for and switch
*/
package main

import (
	"log"
	"runtime"
	"strings"
	"time"

	sup "gitlab.com/nagypalg/gotour4javadevs/pkg/support"
)

// notice how nicely the import block is formatted
// (alphabetic ordering, separation of standard library from own package etc.)
// this is due the automatic and quasi-mandatory application of gofmt
// which practically eliminates all discussions about source code conventions
// e.g. gofmt automatically removes all semicolons from line endings if you try to add some

func ifDemo(s string) {
	// Note: no parentheses
	// we can define variables which are visible in the if/else blocks only
	if t := strings.ToUpper(s); t == "GO" {
		log.Println("Yeah, it is Go!")
	} else {
		log.Printf("'%s' is NOT Go", t)
	}
	// Note: t is not visible here
}

func forDemo() {
	// note: there are no parentheses here either
	for i := 0; i < 3; i++ {
		log.Println(i)
	}
}

func forAsWhile() int {
	sum := 1
	// we can omit the init and post statements and keep only the condition
	// so for can be used as while
	// therefore Go does not have while (or do..while)
	for sum < 1000 {
		sum += sum
	}
	return sum
}

func almostInfiniteLoop() {
	// we can omit everything, then it would be an infinite loop
	for {
		// if we would not break
		break
	}
}

func whichOS() string {
	res := "Go runs on "
	// the switch statement can also declare variables visible only in its block
	switch os := runtime.GOOS; os {
	case "darwin":
		res += "OS X."
		// note: no break needed, first matching case is executed only (or default)
	case "linux":
		res += "Linux."
	case "freebsd", "openbsd": // multiple conditions separated by comma
		res += "BSD."
	default:
		// plan9, windows...
		res = res + os + "."
	}
	return res
}

func whenIsSaturday() (string, string) {
	today := time.Now().Weekday()
	var whenIsSaturday string
	switch time.Saturday {
	// note that cases does not have to be constants!
	case today + 0:
		whenIsSaturday = "Today."
	case today + 1:
		whenIsSaturday = "Tomorrow."
	case today + 2:
		whenIsSaturday = "In two days."
	default:
		whenIsSaturday = "Too far away."
	}
	return today.String(), whenIsSaturday
}

func timeAwareGreeting() (now, res string) {
	t := time.Now()
	now = t.String()
	// switch does not need a condition: same as switch true
	// in this case it is a better replacement for long if..else if..else chains
	switch {
	case t.Hour() < 12:
		res = "Good morning!"
	case t.Hour() < 17:
		res = "Good afternoon."
	default:
		res = "Good evening."
	}
	return
}

func main() {
	ifDemo("go")
	sup.PrintSepLog(log.Println)

	ifDemo("Java")
	sup.PrintSepLog(log.Println)

	forDemo()
	sup.PrintSepLog(log.Println)

	log.Println(forAsWhile()) //1024
	sup.PrintSepLog(log.Println)

	almostInfiniteLoop()

	log.Println(whichOS()) //Go runs on OS X. (for me :) )
	sup.PrintSepLog(log.Println)

	today, whenIsSaturday := whenIsSaturday()
	log.Printf("Today: %s, Saturday is %s", today, whenIsSaturday)
	sup.PrintSepLog(log.Println)

	now, greeting := timeAwareGreeting()
	log.Printf("Greeting is '%s' at %s", greeting, now)

}
