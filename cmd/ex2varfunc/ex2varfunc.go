/*
Introduces constants, variables and functions, as well as modules.
*/
package main

// multiple import statements can (and should be) grouped
import (
	"errors"
	"fmt"
	"math"
	"strconv"

	// Since Go 1.12 there are modules
	// Check go.mod, it states:
	// module gitlab.com/nagypalg/gotour4javadevs
	// Therefore all own packages must be prefixed with the module name (gitlab.com/nagypalg/gotour4javadevs)
	// note: we can use package aliases. We can refer to this package as "sup" instead of "support"
	sup "gitlab.com/nagypalg/gotour4javadevs/pkg/support"
)

// constant declarations: only for built-in types: numeric, string, bool
// can be grouped
const (
	// note that types are after names, not before as in Java
	myPi float64 = 3.14
	// MyPi is roughly the same as math.Pi
	MyPi = 3.1415
)

// but constants also can be ungrouped
// their type is auto-inferred
const myMessage = "Go rulez!"

// variable definition and be grouped or ungrouped such as constants and imports
var (
	// In Go ALL variables are initialized to their zero values
	// For string the zero value is ""
	// Obviously in this case we have to define the type
	myString string
	// but we can also give them a value right away, in this case the type definition is optional
	// as a matter of fact, it is best practice to omit it, if it would be the default (in this case: int)
	// "best practice" means go vet will complain if you don't follow it :)
	myInt = 123
	// if we want to use a different type, we can though
	myInt16 int16 = 456
	// Zero value for numbers is 0
	myFloat float64
	// Zero value for bool is false
	myBool bool
)

// Go has raw strings!
var sql = `select *
from tbl
where col like '%foo%'
`

// functions
func add(x int, y int) int {
	return x + y
}

// Multiple results!
// Parameters with the same type can be grouped
func swap(x, y string) (string, string) {
	return y, x
}

// Named return values!
// These can be treated as variables that are initialized with their zero values
func divide(x int, y int) (res float64, err error) {
	if y == 0 {
		return math.NaN(), errors.New("Cannot divide by zero")
	}
	// note that it is different from float64(x / y) which would be integer division!
	res = float64(x) / float64(y)
	// "naked return", will return the current value of the named parameters
	return
}

// Printf supports lots of formatting options
// v means value and returns a standard representation of all Go types
// T means type and shows the Go type of the value
// see https://golang.org/pkg/fmt/ for a complete documentation
func printFloat64(v float64) {
	fmt.Printf("%v: %T\n", v, v)
}

// Note that this is a procedure, i.e. does not return anything (void in Java)
func printInt(v int) {
	fmt.Printf("%v: %T\n", v, v)
}

func main() {
	// Println will separate the values with a space
	fmt.Println(myPi, MyPi, myInt, myInt16, myFloat, strconv.Quote(myString), myBool, strconv.Quote(myMessage))
	// 3.14 3.1415 123 456 0 "" false "Go rulez!"
	sup.PrintSepFmt()

	// note: we use Print instead of Println because the raw string already has a newline at the end
	fmt.Print(sql)
	sup.PrintSepFmt()

	// In go we don't have implicit type conversions!
	// We have to say that we want to treat myInt16 as int even if we know that it is a subclass of int
	// (in most systems int is an alias for int64)
	// 'added :=' is a shortcut for 'var added =', and it an be used only in function bodies
	added := add(myInt, int(myInt16))
	fmt.Println(added) // 579
	sup.PrintSepFmt()

	var zero int
	two := 2

	// multiple assignment
	div, err := divide(myInt, zero)
	// It is a very common pattern for error handling (Go has no exceptions!)
	if err != nil {
		fmt.Println("Error:", err.Error()) //Error: Cannot divide by zero
	} else {
		fmt.Println(div) //never executed
	}
	sup.PrintSepFmt()

	// we can ignore return values (we have to assign all return values!)
	div, _ = divide(myInt, two)
	fmt.Println(div) // 61.5
	sup.PrintSepFmt()

	// we can also ignore the first value (or both if we want)
	_, err = divide(myInt, two)
	fmt.Println(err) // <nil> - zero value for error is nil
	sup.PrintSepFmt()

	s1, s2 := swap("World", "Hello")
	fmt.Printf("%s %s\n", s1, s2) //Hello World
	sup.PrintSepFmt()

	var ip *int // There are pointers! Zero value: nil
	// but luckily there is no pointer arithmetic, like in C/C++
	fmt.Println(ip) // <nil>
	sup.PrintSepFmt()

	ip = &added
	fmt.Printf("%v points to %v\n", ip, *ip) // 0xc00008e018 points to 579
	// note: your pointer address most likely will be different
	sup.PrintSepFmt()

	// Constants can be character, string, boolean, or numeric values.
	// An untyped constant takes the type needed by its context.
	const two100c = 1 << 100
	printFloat64(two100c) // 1.2676506002282294e+30: float64
	// printInt(two100c) would overflow int!
	sup.PrintSepFmt()

	const two3c = 1 << 3
	printInt(two3c) // 8: int
	sup.PrintSepFmt()
	printFloat64(two3c) // 8: float64
}
