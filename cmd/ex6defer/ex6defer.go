/*
Demoes defer
*/
package main

import (
	"fmt"
	"io"
	"os"

	sup "gitlab.com/nagypalg/gotour4javadevs/pkg/support"
)

func countWithDefer() {
	fmt.Print("counting")

	// defer executes the function call only when the current function exists
	// it is like a finally clause in Java on steroids
	defer fmt.Println()

	for i := 0; i < 10; i++ {
		// parameters of the deferred function call are evaluated immediately
		// multiple defer calls are stacked and the last one is executed first (i = 9)
		defer fmt.Print(i, " ")
	}

	fmt.Print(" with defer: ")
}

// shows typical error handling and usage of defer, instead of using Exceptions
func copyFile(dstName, srcName string) (written int64, err error) {
	src, err := os.Open(srcName)
	if err != nil {
		return
	}
	defer src.Close()
	// Compare this with try..catch..finally
	// the main difference: try and catch/finally are not separated from each other
	// so it is easy to see that we have not forgotten about error handling and cleanup

	dst, err := os.Create(dstName)
	if err != nil {
		return
	}
	defer dst.Close()

	return io.Copy(dst, src)
	// due to the defer calls, both dst and src will be properly closed, it does not matter what happens
	// note that we did not use a naked return here, so naming return values can also serve documentation purposes only
}

func handleCopyFile(src, dest string) {
	written, err := copyFile(dest, src)
	if err != nil {
		fmt.Println("Error:", err.Error())
	} else {
		fmt.Printf("Written %d bytes\n", written)
	}
}

func main() {
	countWithDefer() // counting with defer: 9 8 7 6 5 4 3 2 1 0
	sup.PrintSepFmt()

	handleCopyFile("src.txt", "dest.txt") // Written 30 bytes
	sup.PrintSepFmt()

	handleCopyFile("nonexistent.txt", "dest2.txt")
	// Error: open nonexistent.txt: no such file or directory

}
